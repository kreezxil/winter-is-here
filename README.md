![Winter is Here](https://i.imgur.com/sOFjY1N.png)
# Winter is Here

The Kingdoms have long been ravaged. The dragons have all returned to deep subterranean abodes. You're job is to reunite the land, to bring it under your iron-fisted control and for this you will need a fierce dragon of your own!